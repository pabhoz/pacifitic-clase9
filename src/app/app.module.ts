import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
//Módulo para habilitar el ngModel en inputs
import { FormsModule } from '@angular/forms';
//Enrutador para paginación
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { LazyComponent } from './lazy/lazy.component';
import { CalculadoraComponent } from './calculadora/calculadora.component';
import { CuadernoComponent } from './cuaderno/cuaderno.component';
import { HomeComponent } from './home/home.component';

const rutasApp: Routes = [
  {
    path: 'home', component: HomeComponent
  },
  {
    path: 'lazy', component: LazyComponent
  },
  {
    path: 'calculadora', component: CalculadoraComponent
  },
  {
    path: '',
    redirectTo: '/home',
    pathMatch: 'full'
  },
  //{ path: '**', component: NotFoundComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    LazyComponent,
    CalculadoraComponent,
    CuadernoComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule.forRoot(
      rutasApp,
      { enableTracing: false } // <-- debugging purposes only
    )
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
